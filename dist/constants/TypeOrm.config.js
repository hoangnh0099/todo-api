"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
exports.typeOrmConfig = {
    type: 'mysql',
    host: process.env.DB_HOST || 'localhost',
    port: Number(process.env.DB_PORT) || 3306,
    username: process.env.DB_USERNAME || 'root',
    password: process.env.DB_PASSWORD || '12345678',
    database: process.env.DB_NAME || 'test',
    entities: ['dist/**/*.entity{.ts,.js}'],
    synchronize: true,
    logging: true,
};
//# sourceMappingURL=TypeOrm.config.js.map